# Practical

First open GitLab (or GitHub) and go to your fork of the project. I'm going to be using GitLab, GitHub will have all the same features it'll just look different.

The project overview page will be on master branch by default, and will show you the files and directories of this branch of the project, with an indication of the latest commit that modified each.

## The README

Notice that the [README.md](README.md) file, which is just a markdown file saved in the root directory of the project, is automatically rendered on the homepage of the repository.

This is a really useful place to put any key information about the project that you want people to see straight away - for example often the remote repository for a software module will have installation instructions here, and maybe a vignette.


For today's example, we're going to use the [PracticeMerge.md](PracticeMerge.md) file, in the root directory of the project. Click on it to have a look - and note that again, GitLab has automatically rendered the Markdown content for us.

This practice document contains a recipe - and you'll notice that the units of measurement here are in ounces. We think that metric units would be more useful, and we want to flag this as a change to be made.

We're going to do this by creating an "Issue". 

## Creating an Issue

In the left toolbar, click on "Issues" (narrow screen, might just be an icon) and then select the blue button for "New issue". 

This opens up a form to create an issue - we can give it a title, and add a description.  

> **Example**: Say we've submitted this recipe to a peer-reviewed magazine and gets reviewer comments back asking for a particular sensitivity analysis to be done. The indivdual who received the reviewer comments could make an Issue for this and use the description field to specify the desired analyses. They could specify a due date based on the deadline for resubmitting the recipe, categorise it as a quick task with a low "weight", or label it as being review related. They can then assign the task to one of the analysts on their team - and the analyst can comment on the issue to discuss any questions relating to the implementation. 

The issue functionality makes a lot of tools for project management available to you, and you can choose which are appropriate and helpful for your team.

This functionality is similar to what you can find in other project management tools, but it has the benefit of being integrated into your remote repository, and we'll have a look at some of the cool stuff you can do with that later.

Ok, so we've made an issue indicating that we want to change our units of measurement to metric. In the real world, there would now be a time lag while somebody went away and implemented this, but because this is a practical session, here's one I prepared earlier!

## Viewing other branches

If we go to Repository -> Graph in the left toolbar, we can see a git graph for the project - similar to how it is presented in VS Code.

So here are all the commits of me setting up this repository, and we see that after the recipe was created on master branch, there were two branches - one for imperial units and one for metric units. The imperial units branch was merged into master, resulting in the version of the recipe with imperial units that we saw a minute ago. 

We're interested in the `metric` branch, which has one commit. If we click on that commit in the graph we can inspect the changes made in this commit - in this case, it adds metric units to the original recipe.

* Merge request - metric into master
    * Select metric branch
    * Careful to select _your_ project
    * Reference Issue #1 in description
    * Assign to self
* Look at merge request
    * Follow link to issue, show that issue now shows that a merge request is addressing it
    * Comment on it - back to home page of merge request, show discussion thread
        * Note options to resolve discussion thread and create new issue addressing it
* Request cannot be merged because conflicts
    * Click on button to resolve online, show what it looks like, explain better to resolve locally so we can test changes
    * Click on button to resolve locally, show that it gives the necessary git commands to do this in terminal
* We're going to resolve locally in VS Code because strongly recommend always using a visual editor to deal with conflicts
* Open VS Code
    * Navigate to local repo
    * Go to version control panel, open Git Graph
    * Git fetch (just because)
* Discussion of how remote branches show on Git Graph
    * Toggle "Show remote branches" - if lots of collaborators, may want to hide this sometimes to focus on your own branches
    * The "origin" label
    * origin/bname = only on origin
    * bname = only on local
    * bname | origin = on both
* Checkout metric
    * Right click on the label not on the commit
    * VS code prompts that this doesn't exist locally, creates new branch
    * Doesn't take long - doesn't need to download changes, already downloaded by fetch
* Merge master into metric
    * Explain - can't merge metric into master locally, can't push to master
    * Better to merge master into metric here so we can test code locally after resolving conflicts
    * Right click on label of master, select "Merge into current branch..."
    * We previously discussed fast-forward merge, squash commits
    * Stick with defaults
* Merge conflict
    * Dismiss pop-up
    * Open conflict file - notice "!" indicates conflict
    * If we try to stage with conflict, it gives us a warning pop-up
    * See colour-coded chunks
* Resolving conflict
    * First chunk: want to keep metric units
    * Second chunk: two different method tips, keep whichever you think more useful - or both!
    * Third chunk: incoming change has more method tips, maybe keep that one
* Save file with File, Save
* Stage file
* Commit
    * Prepopulated with default merge commit message
    * Can change this manually or stick with default
    * Ctrl + Enter to commit (or Command + Enter on Mac)
* Back to Git graph
    * See graphical depiction of merge
    * See that local metric branch is now ahead of remote metric branch
* Push metric branch
    * VS Code checks that we want to set upstream, because we haven't pushed from this branch before
* Back to GitLab
    * View Graph - changes are here
    * Merge request - now 2 commits, and able to merge
    * Notice approval section - depending on project settings, this is where approve action would be. In this case, no action is necessary, green tick!
* Merge
    * Merge request page still exists - shows who performed the merge and hash of post-merge commit
    * Go back to merge request overview page - it's not showing a count of 0 MR 
    * We can see the "Merged" tab has the record of the one we just merged
    * Back to graph, look at the merge
* Back to VS Code
    * Fetch changes
    * Checkout master (triple dots, "Checkout to..", master)
    * Pull changes from remote (triple dots, pull)
* Tag the finished recipe
    * Right click on commit (not on label), "Add tag..."
    * Pop-up:
        * Tag name (no spaces)
        * Message (optional)
        * Push to remote - convenient that VS Code lets us do this all-in-one, would be a separate command in terminal
* Back to GitLab, see tag appeared on graph
* Back to VS Code
    * Create random commit with blank file
    * See that branch moves on, tag doesn't